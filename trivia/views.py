from django.shortcuts import render
from django.http import HttpResponse
from django.urls import reverse_lazy
from django.views.generic.base import TemplateView
from .models import Book, Question
from django.views.generic.edit import UpdateView, DeleteView, CreateView
from .forms import QuestionForm
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
import random
# Create your views here.

class HomePageView(TemplateView):
    template_name = "index.html"

class AgeSelectView(TemplateView):
    template_name = "select-age-range.html"

class TriviaAdminView(LoginRequiredMixin, TemplateView):
    template_name = "trivia-admin.html"
    login_url = 'login'

@login_required(login_url='login')
def get_books(request):
    all_books = Book.objects.all()
    context = {
        'all_books': all_books
    }
    return render(request, 'books-list.html', context)

def get_books_by_age(request, age):
    books_by_age = Book.objects.filter(age_range = age)
    context = {
        'books_by_age': books_by_age
    }
    return render(request, 'books-by-age.html', context)

def get_random_books(request, age):
    books_by_age = Book.objects.filter(age_range = age)
    books_id = list()
    for book in books_by_age:
        books_id.append(book.id)
    wanted_books = get_random_set(books_id)
    random_books = Book.objects.filter(id__in = wanted_books)

    context = {
        'random_books': random_books
    }
    return render(request, 'books-by-age.html', context)

def get_book_questions(request, pk):
    book_questions = Question.objects.filter(book_id = pk)
    context = {
        'book_questions': book_questions,
    }
    return render(request, 'book-questions.html', context)

def get_question_detail(request,pk):
    question_detail = Question.objects.get(id = pk)
    context = {
        'question_detail': question_detail
    }
    return render(request, 'question-detail.html', context)

def get_question_detail_random(request, pk):
    question_detail = Question.objects.get(id = pk)
    options = [question_detail.option1, question_detail.option2, question_detail.option3]
    random.shuffle(options)
    option1 = options[0]
    option2 = options[1]
    option3 = options[2]
    context = {
        'question_detail': question_detail,
        'option1': option1,
        'option2': option2,
        'option3': option3,
    }
    return render(request, 'question-detail.html', context)


# INSERT UPDATE AND DELETE FUNCTIONS FOR BOOK
class BookCreateView(LoginRequiredMixin, CreateView):
    model = Book
    template_name = 'book-new.html'
    fields = ('title', 'author', 'age_range')
    login_url = 'login'

class BookUpdateView(LoginRequiredMixin, UpdateView):
    model = Book
    template_name = 'book-edit.html'
    fields = ('title', 'author', 'age_range')
    login_url = 'login'

class BookDeleteView(LoginRequiredMixin, DeleteView):
    model = Book
    template_name = 'book-delete.html'
    success_url = reverse_lazy('trivia_admin')
    login_url = 'login'

@login_required(login_url='login')
def get_book_options(request, pk):
    book_detail = Book.objects.get(id = pk)
    context = {
        'book_detail': book_detail
    }
    return render(request, 'book-options.html', context)


# INSERT, UPDATE AND DELETE FUNCTIONS FOR QUESTIONS
# class QuestionCreateView(CreateView):
#     model = Question
#     template_name = 'question-new.html'
#     fields = ('question', 'option1', 'option2','option3', 'book')

@login_required(login_url='login')
def question_create(request, pk):
    form = QuestionForm(request.POST or None)
    if form.is_valid():
        question = form.cleaned_data.get('question')
        option1 = form.cleaned_data.get('option1')
        option2 = form.cleaned_data.get('option2')
        option3 = form.cleaned_data.get('option3')
        book_id = pk
        q = Question(question = question, option1 = option1, option2 = option2, option3 = option3, book_id = book_id)
        q.save()
    context = {
        'form': form,
    }
    return render(request, 'question-create.html', context)

def get_questions_by_book(request, pk):
    book = Book.objects.get(id=pk)
    questions = Question.objects.filter(book_id = pk)
    context = {
        'questions':questions,
        'book':book,
    }
    return render(request, 'questions-list.html', context)

def get_questions_options(request, pk):
    book_info = Book.objects.get(id = pk)
    context = {
        'book_info':book_info
    }
    return render(request, 'questions-options.html', context)

def get_question_option():
    pass

@login_required(login_url='login')
def questions_by_book(request, pk):
     questions_by_book = Question.objects.filter(book_id = pk)
     context = {
        'questions_by_book': questions_by_book
     }
     return render(request, 'questions-by-book.html', context)

class QuestionUpdateView(LoginRequiredMixin, UpdateView):
    model = Question
    template_name = 'question-edit.html'
    fields = ('question', 'option1', 'option2', 'option3')
    login_url = 'login'

def get_question_options(request, pk):
    question_detail = Question.objects.get(id = pk)
    context = {
        'question_detail': question_detail
    }
    return render(request, 'question-options.html', context)

class QuestionDeleteView(LoginRequiredMixin, DeleteView):
    model = Question
    template_name = 'question-delete.html'
    success_url = reverse_lazy('trivia_admin')
    login_url = 'login'

# Other functions
def get_random_set(books_id):
    wanted = set()
    i = 0
    while (i < 3):
        number = random.choice(books_id)
        if (number not in wanted):
            wanted.add(number)
            i = i + 1
    return wanted
