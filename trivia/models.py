from django.db import models
from django.urls import reverse

# Create your models here.

class Book(models.Model):
    title = models.CharField(max_length=1000)
    author = models.CharField(max_length=1000)
    age_range = models.CharField(max_length=1)

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse('trivia_admin')

class Question(models.Model):
    question = models.CharField(max_length=1000)
    option1 = models.CharField(max_length=1000)
    option2 = models.CharField(max_length=1000)
    option3 = models.CharField(max_length=1000)
    # right_answer = models.CharField(max_length=1000)
    book = models.ForeignKey(Book, on_delete = models.CASCADE)

    def get_absolute_url(self):
        return reverse('trivia_admin')
