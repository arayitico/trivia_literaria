from django import forms
from .models import Question


class QuestionForm(forms.Form):
    question = forms.CharField(max_length= 1000, label = 'Pregunta')
    option1 = forms.CharField(max_length= 1000, label = 'Opcion 1 (Respuesta Correcta)')
    option2 = forms.CharField(max_length= 1000, label = 'Opcion 2')
    option3 = forms.CharField(max_length= 1000, label = 'Opcion 3')
