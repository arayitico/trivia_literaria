from django.shortcuts import render
from django.views.generic import CreateView
from .forms import CustomUserCreationForm
from django.views.generic.base import TemplateView



# Create your views here.


class SignUpView(CreateView):
    form_class = CustomUserCreationForm
    success_url = ('success-signup') # pending to assign a 'success url'
    template_name = 'signup.html'

class SuccessSignUpPageView(TemplateView):
    template_name = "success-signup.html"
