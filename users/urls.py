from django.urls import path
from .views import SignUpView, SuccessSignUpPageView

urlpatterns = [
    path('signup/', SignUpView.as_view(), name = 'signup'),
    path('signup/success-signup/', SuccessSignUpPageView.as_view(), name = 'success_signup'),
]
